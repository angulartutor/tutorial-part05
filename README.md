# Tutorial: Routing

Part 5 of the Angular Official Tutorial,
using sample app "Tour of Villains" (parody of "Tour of Heroes").

This part is based on the previous lessons, Parts 1 ~ 4.
To get started, you can just clone this repo,
or you can start from scratch and make necessary changes based on the tutorial.




* [Tutorial - Routing](https://angular.io/tutorial/toh-pt5)


