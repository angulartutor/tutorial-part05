import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import { Villain } from './villain';
import { VILLAINS } from './mock-villains';
import { MessageService } from './message.service';


@Injectable()
export class VillainService {

  constructor(private messageService: MessageService) {

  }

  getVillains(): Observable<Villain[]> {

    this.messageService.add('VillainService: fetched villain list.');

    return of(VILLAINS);
  }

  getVillain(id: number): Observable<Villain> {
    // ????
    return this.getVillains()
    .map(villains => villains.find(villain => villain.id === id));
  }

}
