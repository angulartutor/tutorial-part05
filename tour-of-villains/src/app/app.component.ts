import { Component, OnInit } from '@angular/core';
import { Villain } from './villain';
import { VillainService } from './villain.service';
import { VillainsComponent } from './villains/villains.component';


@Component({
  selector: 'app-root',
  template: `
  <h1>{{title}}</h1>

  <!--
  <my-villains></my-villains>
  -->

  <nav>
  <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
  <a routerLink="/villains" routerLinkActive="active">Villains</a>
  </nav>
  <router-outlet></router-outlet>

  `,
  styles: [
    `
    h1 {
      font-size: 1.2em;
      color: #999;
      margin-bottom: 0;
    }
    h2 {
      font-size: 2em;
      margin-top: 0;
      padding-top: 0;
    }
    nav a {
      padding: 5px 10px;
      text-decoration: none;
      margin-top: 10px;
      display: inline-block;
      background-color: #eee;
      border-radius: 4px;
    }
    nav a:visited, a:link {
      color: #607D8B;
    }
    nav a:hover {
      color: #039be5;
      background-color: #CFD8DC;
    }
    nav a.active {
      color: #039be5;
    }
    `
  ]
})
export class AppComponent implements OnInit {
  title = 'Tour of Villains';

  constructor() {
  }

  ngOnInit(): void {
  }

}
