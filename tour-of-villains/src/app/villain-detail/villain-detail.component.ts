import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { Villain } from '../villain';
import { VillainService } from '../villain.service';

@Component({
  selector: 'villain-detail',
  templateUrl: './villain-detail.component.html',
  styleUrls: ['./villain-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VillainDetailComponent implements OnInit {
  @Input()
  villain: Villain;

  constructor(
    private villainService: VillainService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.villainService
        .getVillain(+params.get('id')))
      .subscribe(villain => this.villain = villain);
  }

  goBack(): void {
    this.location.back();
  }

}
