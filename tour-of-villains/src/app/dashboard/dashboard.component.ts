import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Villain } from '../villain';
import { VillainService } from '../villain.service';

@Component({
  selector: 'my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  villains: Villain[] = [];

  constructor(private villainService: VillainService) { }

  ngOnInit(): void {
    this.villainService.getVillains()
    .subscribe(villains => this.villains = villains.slice(1,5));
  }

}
